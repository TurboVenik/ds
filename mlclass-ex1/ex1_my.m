clear all; close all; clc

fprintf('Loading data ...\n');
%% Load Train Data
train = load('train');
X = train(:, 1:10);
y = train(:, 11);
m = length(y);

fprintf('Top 10.\n');
disp([X(1:10,:) y(1:10,:)]);

% Add new params
X = moreParams(X);
% Normalize
[X mu sigma] = featureNormalize(X);
% Add column (1,1,1,1...) as a first column
X = [ones(m, 1) X];

fprintf('Running gradient descent ...\n');

% Max alpha value
alpha_max = 0.05;
% Min alpha value
alpha_min = 0.00001;
% How much times alpha became max
epo = 10;
% how many times does alpha decrease
num_iters = 10;
% iterations on each alpha value
change_t_iters = 100;

% Init Theta and Run Gradient Descent 
theta = zeros(size(X,2), 1);
[theta, J_history] = gradientDescentMultiMy(X, y, theta, alpha_max, alpha_min, num_iters, epo, change_t_iters);

% Plot the convergence graph
figure;
plot(1:numel(J_history), J_history, '-b', 'LineWidth', 2);
xlabel('Number of iterations');
ylabel('Cost J');

%% Load Test Data
train = load('test');

test_X = train(:, 1:10);
test_y = train(:, 11);

test_m = length(test_y);

% Add new params
test_X = moreParams(test_X);
% Normalize
test_X = test_X - repmat(mu, size(test_X, 1), 1);
test_X = test_X ./ repmat(sigma, size(test_X, 1), 1);
% Add column (1,1,1,1...) as a first column
test_X = [ones(test_m, 1) test_X];
% Calculate answer
answer = test_X * theta;
% Calculate error in %
my_error = abs((test_y(1:40,:) - answer(1:40,:)) ./ test_y(1:40,:) .* 100);

fprintf('train J  ....  test J\n');
disp([computeCost(X, y, theta) computeCost(test_X, test_y, theta)])

disp("mean error %");
disp(mean(my_error));

fprintf('         My     Actual           my_error \n');
disp([ answer(1:40,:) test_y(1:40,:) my_error]); 
