function [theta, J_history] = gradientDescentMultiMy(X, y, theta, alpha_max, alpha_min, num_iters, epo, change_t_iters)

  m = length(y); % number of training examples
  dt = (alpha_max - alpha_min) / num_iters

  number = 0;
  min = computeCostMulti(X, y, theta);
  min_theta = theta;
  for iter = 1:epo
      t = alpha_max;
      v = ((rand(1, size(theta, 1)))' - repmat(0.5, size(theta, 1), 1));
      v = v ./ norm(v);
      theta = theta - (40) * v;
      for iter1 = 1:num_iters
        t = t - dt;
        
        for iter2 = 1:change_t_iters
          number += 1;
          h = X * theta;
          if mod(number, 100) == 0 && number > 1400
            v = ((rand(1, size(theta, 1)))' - repmat(0.5, size(theta, 1), 1));
            v = v ./ norm(v);
            theta = theta - (40) * v;
          end
          theta = theta - (t / m) * (X' * (h - y));
          
          cost = computeCostMulti(X, y, theta);
         
          if cost <  min
            min = cost;
            min_theta = theta;
          end 
          
     
          J_history(number) = cost;
        end
        
        for iter2 = 1:200
          number += 1;
          h = X * theta;
          theta = theta - (t / m) * (X' * (h - y));
          cost = computeCostMulti(X, y, theta);
          if cost <  min
            min = cost;
            min_theta = theta;
          end 
          J_history(number) = cost;
        end
      end
  end
  theta = min_theta;
end

 