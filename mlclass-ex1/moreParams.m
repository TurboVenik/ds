function [X] = moreParams(X)
%NORMALEQN Computes the closed-form solution to linear regression 
%   NORMALEQN(X,y) computes the closed-form solution to linear 
%   regression using the normal equations.

theta = zeros(size(X, 2), 1);

% ====================== YOUR CODE HERE ======================
% Instructions: Complete the code to compute the closed form solution
%               to linear regression and put the result in theta.
%

% ---------------------- Sample Solution ----------------------

m = size(X, 2);

for iter1 = 1:m
    for iter2 = iter1:m
         X = [X X(:, iter1).*X(:, iter2)];
      for iter3 = iter2:m
         X = [X X(:, iter1).*X(:, iter2).*X(:, iter2)];
      end
    end
end

% -------------------------------------------------------------


% ============================================================

end
