import random


def f(arr):
    return 5 * arr[0] * arr[0] + 2 * arr[1] * arr[2] - 5 * arr[9] * arr[1] + arr[3] * arr[4] * arr[4] - 3 * arr[5] * arr[5] * arr[5] + 4 * arr[6] + arr[7] * arr[8] * arr[9] + 2 * arr[0] * arr[5] * arr[7]


with open("train", "w") as file:
    for j in range(1000):
        arr = []
        for i in range(10):
            arr.append((random.randint(0, 100)/10))

        for item in arr:
            file.write(str(item) + ",")
        file.write(str(f(arr)) + "\n")
        print(arr)
        print(f(arr))

with open("test", "w") as file:
    for j in range(500):
        arr = []
        for i in range(10):
            arr.append((random.randint(0, 100)/10))

        for item in arr:
            file.write(str(item) + ",")
        file.write(str(f(arr)) + "\n")
        print(arr)
        print(f(arr))
